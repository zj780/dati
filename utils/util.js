const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}


const timestampToTime = timestamp => {
  var time = new Date(timestamp);
  var y = time.getFullYear();
  var m = time.getMonth() + 1;
  var d = time.getDate();
  var h = time.getHours();
  var mm = time.getMinutes();
  var s = time.getSeconds();
  return y + '-' + add0(m) + '-' + add0(d) + ' ' + add0(h) + ':' + add0(mm) + ':' + add0(s);
}

const timestampToTimeFramt = timestamp => {
  var time = new Date(timestamp);
  var y = time.getFullYear();
  var m = time.getMonth() + 1;
  var d = time.getDate();
  var h = time.getHours();
  var mm = time.getMinutes();
  var s = time.getSeconds();
  //返回年月日
  return y + "" + add0(m) + "" + add0(d);
}

//日期转时间戳
const timestap = datetime => {
  return parseInt(new Date(datetime.replace(/-/g, "/")).getTime() / 1000);
}

//几分钟前
const diaplayTime = datetime => {
  var publishTime = getDateTimeStamp(datetime) / 1000,
    d_seconds,
    d_minutes,
    d_hours,
    d_days,
    timeNow = parseInt(new Date().getTime() / 1000),
    d,

    date = new Date(publishTime * 1000),
    Y = date.getFullYear(),
    M = date.getMonth() + 1,
    D = date.getDate(),
    H = date.getHours(),
    m = date.getMinutes(),
    s = date.getSeconds();
  //小于10的在前面补0
  if (M < 10) {
    M = '0' + M;
  }
  if (D < 10) {
    D = '0' + D;
  }
  if (H < 10) {
    H = '0' + H;
  }
  if (m < 10) {
    m = '0' + m;
  }
  if (s < 10) {
    s = '0' + s;
  }

  d = timeNow - publishTime;
  d_days = parseInt(d / 86400);
  d_hours = parseInt(d / 3600);
  d_minutes = parseInt(d / 60);
  d_seconds = parseInt(d);

  if (d_days > 0 && d_days < 3) {
    return d_days + '天前';
  } else if (d_days <= 0 && d_hours > 0) {
    return d_hours + '小时前';
  } else if (d_hours <= 0 && d_minutes > 0) {
    return d_minutes + '分钟前';
  } else if (d_seconds < 60) {
    if (d_seconds <= 0) {
      return '刚刚';
    } else {
      return d_seconds + '秒前';
    }
  } else if (d_days >= 3 && d_days < 30) {
    return M + '-' + D + ' ' + H + ':' + m;
  } else if (d_days >= 30) {
    return Y + '-' + M + '-' + D + ' ' + H + ':' + m;
  }
}

function getDateTimeStamp(dateStr) {
  // 如果时间格式为2020/07/09 21:43:19.000  需要去掉.000 不然ios和firefox会有问题
  return Date.parse(dateStr.replace(/-/gi, "/"));
}

function add0(m) {
  return m < 10 ? '0' + m : m
}

//文件大小
/// <summary>
/// 格式化文件大小的JS方法
/// </summary>
/// <param name="filesize">文件的大小,传入的是一个bytes为单位的参数</param>
/// <returns>格式化后的值</returns>
function renderSize(fileSize) {
  if (fileSize < 1024) {
    return fileSize + 'B';
  } else if (fileSize < (1024 * 1024)) {
    var temp = fileSize / 1024;
    temp = temp.toFixed(2);
    return temp + 'KB';
  } else if (fileSize < (1024 * 1024 * 1024)) {
    var temp = fileSize / (1024 * 1024);
    temp = temp.toFixed(2);
    return temp + 'MB';
  } else {
    var temp = fileSize / (1024 * 1024 * 1024);
    temp = temp.toFixed(2);
    return temp + 'GB';
  }
}


//删除数组某一项
/*
  this.openList.some((item, i) => {
    if (item.userId === data.userId) {
        this.openList.splice(i, 1);
    }
  });
*/
function delIndexArr(arr, delIndex) {
  arr.some((item, index) => {
    //删除元素
    if (index === delIndex) {
      arr.splice(delIndex, 1);
    }
  });
  return arr;
}

//获取图片
function getImage(imagesArr, page) {
  for (var index in imagesArr) {
    //获取当前页面图片
    if (imagesArr[index].page == page) {
      return imagesArr[index].url;
    }
  }
}

//公共返回
function backPage() {
  wx.navigateBack({
    delta: 1,
    //成功
    success(success) {
      console.log('返回成功');
    },
    //错误
    fail(err) {
      console.log(err);
      //跳转首页
      wx.reLaunch({
        url: "/pages/index/index"
      });
    }
  });
}

//位数补齐
function PrefixInteger(num, length) {
  return (Array(length).join('0') + num).slice(-length);
}

//用户昵称星号
function formatName(name) {
  var newStr;
  if (name.length === 2) {
    newStr = name.substr(0, 1) + '*';
  } else if (name.length > 2) {
    var char = '';
    for (var i = 0, len = name.length - 2; i < len; i++) {
      char += '*';
    }
    newStr = name.substr(0, 1) + char + name.substr(-1, 1);
  } else {
    newStr = name;
  }
  return newStr;
}

//获取数组随机值
function randomArr(arr) {
  var i = Math.floor(Math.random() * arr.length);
  return arr[i];
}

//获取倒计时
function countDown(end) {
  // end 结束时间
  let start = new Date().getTime(); //当前时间戳
  end = new Date(end).getTime(); //获取结束时间戳
  let time = end - start; //计算相差时间戳
  let d = parseInt(time / 1000 / 60 / 60 / 24); //计算天
  let h = parseInt(time / 1000 / 60 / 60 % 24); //计算时
  let m = parseInt(time / 1000 / 60 % 60); //计算分
  let s = parseInt(time / 1000 % 60); //计算秒
  let countDown = ''; //返回字符串
  if (d > 0) countDown += d + '天';
  if (h > 0) countDown += h + '小时';
  if (m > 0) countDown += m + '分';
  if (s > 0) countDown += s + '秒';
  return countDown;
}


module.exports = {
  formatTime: formatTime,
  timestampToTime: timestampToTime,
  timestampToTimeFramt: timestampToTimeFramt,
  timestap: timestap,
  diaplayTime: diaplayTime,
  delIndexArr: delIndexArr,
  backPage: backPage,
  PrefixInteger: PrefixInteger,
  formatName: formatName,
  randomArr: randomArr,
  countDown: countDown,
  getImage: getImage,
  renderSize: renderSize
}