//单图上传方法(url,参数) 类型:原图模式
async function imageRecursionUpload(url = '', formData = {}) {
  //tempFilePaths为要上传文件资源的路径 (本地路径)，数组类型
  var promise = await new Promise(function (resolve, reject) {
    //打开图片
    wx.chooseMedia({
      count: 1, //单图1张
      mediaType: ['image'], //只限制图片类型
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], //从相册拍照选择
      success: async (result) => {
        //弹窗
        wx.showToast({
          title: '正在上传',
          icon: "loading",
          mask: true,
          duration: 30000
        })
        //图片路径和大小
        const tempFilePath = result.tempFiles[0].tempFilePath;
        let size = result.tempFiles[0].size;
        if (size > 1024 * 1024 * 8) { //大于8M返回
          return wx.showToast({
            title: '图片大于8M啦',
            icon: 'none'
          })
        }
        //开始后台上传
        wx.uploadFile({
          url: url,
          filePath: tempFilePath,
          name: 'file',
          formData: formData,
          success(result) {
            //隐藏加载框
            wx.hideToast();
            //返回图片结果
            var imageResult = JSON.parse(result.data);
            //图片正常
            if (imageResult.status == 200) {
              //回显图片
              resolve(imageResult.data.url);
            } else if (imageResult.status == 87014) {
              //图片违规
              return wx.showToast({
                title: imageResult.info,
                icon: "none",
                duration: 3000
              })
            } else {
              return wx.showToast({
                title: imageResult.info,
                icon: "none",
                duration: 3000
              })
            }
          },
          //异常
          fail: function (error) {
            reject(error);
          }
        })
      }
    })
  });
  return promise;
}
//单图上传方法(url,参数) 类型:原图模式(返回size)
async function imageRecursionSizeUpload(url = '', formData = {}) {
  //tempFilePaths为要上传文件资源的路径 (本地路径)，数组类型
  var promise = await new Promise(function (resolve, reject) {
    //打开图片
    wx.chooseMedia({
      count: 1, //单图1张
      mediaType: ['image'], //只限制图片类型
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], //从相册拍照选择
      success: async (result) => {
        //弹窗
        wx.showToast({
          title: '正在上传',
          icon: "loading",
          mask: true,
          duration: 30000
        })
        //图片路径和大小
        const tempFilePath = result.tempFiles[0].tempFilePath;
        let size = result.tempFiles[0].size;
        if (size > 1024 * 1024 * 8) { //大于8M返回
          return wx.showToast({
            title: '图片大于8M啦',
            icon: 'none'
          })
        }
        //开始后台上传
        wx.uploadFile({
          url: url,
          filePath: tempFilePath,
          name: 'file',
          formData: formData,
          success(result) {
            //隐藏加载框
            wx.hideToast();
            //返回图片结果
            var imageResult = JSON.parse(result.data);
            //图片正常
            if (imageResult.status == 200) {
              //回显图片
              let returnImage = {
                url: imageResult.data.url,
                size: size
              };
              resolve(returnImage);
            } else if (imageResult.status == 87014) {
              //图片违规
              return wx.showToast({
                title: imageResult.info,
                icon: "none",
                duration: 3000
              })
            } else {
              return wx.showToast({
                title: imageResult.info,
                icon: "none",
                duration: 3000
              })
            }
          },
          //异常
          fail: function (error) {
            reject(error);
          }
        })
      }
    })
  });
  return promise;
}
//单图上传裁剪模式
async function tailorImageRecursionUpload(url = '', formData = {}) {
  //tempFilePaths为要上传文件资源的路径 (本地路径)，数组类型
  var promise = await new Promise(function (resolve, reject) {
    //打开图片
    wx.chooseMedia({
      count: 1, //单图1张
      mediaType: ['image'], //只限制图片类型
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], //从相册拍照选择
      success: async (result) => {
        //弹窗
        wx.showToast({
          title: '正在上传',
          icon: "loading",
          mask: true,
          duration: 30000
        })
        //图片路径和大小
        const tempFilePath = result.tempFiles[0].tempFilePath;
        let size = result.tempFiles[0].size;
        if (size > 1024 * 1024 * 8) { //大于8M返回
          return wx.showToast({
            title: '图片大于8M啦',
            icon: 'none'
          })
        }
        //待上传图片
        let editImg = '';
        //判断图片裁剪是否可用
        if (wx.canIUse('editImage')) {
          //可用
          editImg = await editImage(tempFilePath);
        } else {
          //不可用
          editImg = tempFilePath;
        }
        //开始后台上传
        wx.uploadFile({
          url: url,
          filePath: editImg,
          name: 'file',
          formData: formData,
          success(result) {
            //隐藏加载框
            wx.hideToast();
            //返回图片结果
            var imageResult = JSON.parse(result.data);
            //图片正常
            if (imageResult.status == 200) {
              //回显图片
              resolve(imageResult.data.url);
            } else if (imageResult.status == 87014) {
              //图片违规
              return wx.showToast({
                title: imageResult.info,
                icon: "none",
                duration: 3000
              })
            } else {
              return wx.showToast({
                title: imageResult.info,
                icon: "none",
                duration: 3000
              })
            }
          },
          //异常
          fail: function (error) {
            reject(error);
          }
        })
      }
    })
  });
  return promise;
}

//裁剪图片
async function editImage(tempFilePath) {
  //调用图片裁剪插件
  return await new Promise(function (resolve, reject) {
    wx.editImage({
      src: tempFilePath, // 图片路径
      success: function (res) {
        //处理完成
        resolve(res.tempFilePath);
      },
      fail: function (err) {
        //错误消息
        reject(err)
      },
    })
  })
}

//对外暴露
module.exports = {
  imageRecursionUpload: imageRecursionUpload, //单图上传 非裁剪模式
  tailorImageRecursionUpload: tailorImageRecursionUpload, //单图上传 裁剪模式
  imageRecursionSizeUpload: imageRecursionSizeUpload, //单图上传 非裁剪模式 (返回size)
}