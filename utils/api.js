function post(url, params, tips, icon, onSuccess, onFailed) {
  request(url, params, tips, icon, "POST", onSuccess, onFailed);
}

function get(url, params, tips, icon, onSuccess, onFailed) {
  request(url, params, tips, icon, "GET", onSuccess, onFailed);
}

function upImg(url, params, fileName, filePath, tips, icon, onSuccess, onFailed) {
  uploadimg(url, params, fileName, filePath, tips, icon, "POST", onSuccess, onFailed);
}

function upFile(url, params, fileName, filePath, tips, icon, onSuccess, onFailed) {
  uploadfile(url, params, fileName, filePath, tips, icon, "POST", onSuccess, onFailed);
}

//请求域名
var BaseUrl = 'http://127.0.0.1:89';


//上传文件
function uploadfile(url, params, fileName, filePath, tips, icon, method, onSuccess, onFailed) {
  //是否显示加载框
  if (tips) {
    wx.showToast({
      title: tips,
      icon: icon,
      mask: true,
      duration: 30000
    });
  }
  var fileParams = '?';
  for (var index in params) {
    fileParams += index + '=' + params[index] + '&';
  }
  fileParams = (fileParams.substr(0, fileParams.length - 1)).replace(/#/g, '%23');
  //开始上传
  wx.uploadFile({
    url: BaseUrl + url + fileParams, //仅为示例，非真实的接口地址
    filePath: filePath,
    name: fileName,
    formData: params,
    success(res) {
      //隐藏加载框...
      if (tips) {
        wx.hideToast();
      }
      if (res) {
        /** start 根据需求 接口的返回状态码进行处理，根据需要处理不同的状态 */
        onSuccess(res);
        /** end 处理结束*/
      } else {
        //上传失败undefined
        return wx.showToast({
          title: '上传失败啦，请重新上传',
          duration: 1000,
          icon: 'none',
          mask: true
        });
      }
    },
    fail: function (error) {
      onFailed(error); //failure for other reasons
    }
  })
}

//上传
function uploadimg(url, params, fileName, filePath, tips, icon, method, onSuccess, onFailed) {
  //是否显示加载框
  if (tips) {
    wx.showToast({
      title: tips,
      icon: icon,
      mask: true,
      duration: 30000
    });
  }
  var fileParams = '?';
  for (var index in params) {
    fileParams += index + '=' + params[index] + '&';
  }
  fileParams = (fileParams.substr(0, fileParams.length - 1)).replace(/#/g, '%23');
  //开始上传
  wx.uploadFile({
    url: BaseUrl + url + fileParams, //仅为示例，非真实的接口地址
    filePath: filePath,
    name: fileName,
    formData: params,
    success(res) {
      //隐藏加载框...
      if (tips) {
        wx.hideToast();
      }
      if (res) {
        /** start 根据需求 接口的返回状态码进行处理，根据需要处理不同的状态 */
        onSuccess(res);
        /** end 处理结束*/
      } else {
        //上传失败undefined
        return wx.showToast({
          title: '上传失败啦，请重新上传',
          duration: 1000,
          icon: 'none',
          mask: true
        });
      }
    },
    fail: function (error) {
      onFailed(error); //failure for other reasons
    }
  })
}
/**
 * function: 封装网络请求
 * @url URL地址
 * @params 请求参数
 * @method 请求方式：GET/POST
 * @onSuccess 成功回调
 * @onFailed  失败回调
 */

function request(url, params, tips, icon, method, onSuccess, onFailed) {
  //是否显示加载框
  if (tips) {
    wx.showToast({
      title: tips,
      icon: icon,
      mask: true,
      duration: 30000
    });
  }
  var header = {
    "Content-Type": "application/json"
  }
  //发起请求
  wx.request({
    url: BaseUrl + url,
    data: dealParams(params),
    method: method,
    header: header,
    success: function (res) {
      //隐藏加载框...
      if (tips) {
        wx.hideToast();
      }
      if (res) {
        console.log(res);
        /** start 根据需求 接口的返回状态码进行处理，根据需要处理不同的状态 */
        onSuccess(res);
        /** end 处理结束*/
      }
    },
    fail: function (error) {
      console.log(error);
      onFailed(error); //failure for other reasons
    }
  })
}

/**
 * function: 根据需求处理请求参数：添加固定参数配置等
 * @params 请求参数
 */
function dealParams(params) {
  // console.log("请求参数:", params)
  return params;
}


// 1.通过module.exports方式提供给外部调用
module.exports = {
  postRequest: post,
  getRequest: get,
  uploadImg: upImg,
  uploadFile: upFile,
}