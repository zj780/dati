// app.js
App({
  onLaunch: function () {
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力');
    } else {
      wx.cloud.init({
        // env 参数说明：
        //   env 参数决定接下来小程序发起的云开发调用（wx.cloud.xxx）会默认请求到哪个云环境的资源
        //   此处请填入环境 ID, 环境 ID 可打开云控制台查看
        //   如不填则使用默认环境（第一个创建的环境）
        // env: 'my-env-id',
        traceUser: true,
      });
    }
    //系统参数
    wx.getSystemInfo({
      success: res => {
        this.globalData.StatusBar = res.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule;
          this.globalData.CustomBar = capsule.bottom + capsule.top - res.statusBarHeight;
        } else {
          this.globalData.CustomBar = res.statusBarHeight + 50;
        }
      }
    })
  },
  //配置
  config() {
    // 登录
    return new Promise((resolve, reject) => {
      //自己的业务，可能是 异步请求服务端的，如果是异步请求的就请求成功后 resolve(res)
      const db = wx.cloud.database(); // 云数据库初始化
      //加载config
      db.collection('config').limit(1).get().then(res => {
        //赋值
        resolve(res);
      }).catch(err => {
        console.log(err);
      })
    })
  },
  //执行
  appInt() {
    return this.config().then(obj => {
      return obj
    });
  },
  //公共变量
  globalData: {}
});