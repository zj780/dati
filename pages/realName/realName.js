// pages/realName/realName.js
const app = getApp();
import util from '../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    backgroundImage: '', //背景图片
    username: '', //用户姓名
    phone: '', //用户号码
    isLogin: false, //默认未登录
    openid: '', //openid
    avatarUrl: '', //用户头像
    nickName: '', //用户昵称
    userData: {}, //用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var that = this;
    //赋值背景图
    that.setData({
      backgroundImage: util.getImage(app.globalData.images, 'realName')
    })
  },

  //输入姓名
  shuruName: function (event) {
    this.setData({
      username: event.detail.value
    })
  },

  //输入号码
  shuruPhone: function (event) {
    this.setData({
      phone: event.detail.value
    })
  },

  //保存个人信息
  save: function () {
    var that = this;
    //检测姓名是否输入
    if (!that.data.username) {
      wx.showToast({
        title: '请输入姓名',
        icon: "none",
        mask: true,
        duration: 1000
      })
      return;
    }
    //检测手机号码
    if (!that.data.phone) {
      wx.showToast({
        title: '请输入手机号',
        icon: "none",
        mask: true,
        duration: 1000
      })
      return;
    }
    // 判断手机号是否合法  /^[1][3,4,5,7,8][0-9]{9}$/;
    const flag = /^[1][3,4,5,6,7,8,9][0-9]{9}$/;
    //格式不对
    if (!flag.test(that.data.phone)) {
      wx.showToast({
        title: '手机号格式不对',
        icon: "none",
        mask: true,
        duration: 1000
      })
      return;
    }
    //询问框
    wx.showModal({
      title: '系统提示',
      content: '请检查信息是否无误，确认保存吗？',
      success: function (sm) {
        if (sm.confirm) {
          // 用户点击了确定
          that.saveInfo();
        } else if (sm.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  //保存用户姓名和手机号
  saveInfo: async function () {
    var that = this;
    //弹窗
    wx.showToast({
      title: '保存中',
      icon: "loading",
      duration: 30000
    })
    //先检测用户昵称
    const checkResult = await that.checkText();
    if (checkResult.result.errcode == 0) {
      //正常保存入库
      wx.cloud.callFunction({
        name: "saveUser",
        data: {
          openid: that.data.openid,
          username: that.data.username, //姓名
          phone: that.data.phone, //手机号码
          is_info: true, //已填写资料
        },
        success: function (res) {
          //隐藏加载框
          wx.hideToast();
          //保存成功,跳转答题页面
          if (res.result.success === true) {
            wx.showToast({
              title: '保存成功',
              icon: "success",
              duration: 1000
            })
            //延迟1一秒跳转答题页
            setTimeout(function () {
              wx.navigateTo({
                url: "/pages/question/question"
              });
            }, 1000)
          }
        },
        fail: function (res) {
          wx.showToast({
            title: '保存失败',
            icon: "none",
            duration: 1000
          })
        }
      });
    } else {
      //不合法
      wx.showToast({
        title: '姓名不合法，请重新输入',
        icon: "none",
        duration: 1000
      })
    }
  },

  //检测文本
  checkText: function () {
    var that = this;
    //检测文本值
    return new Promise((resolve, reject) => {
      wx.cloud.callFunction({
        name: "checkText",
        data: {
          openid: that.data.openid,
          content: that.data.username
        },
        success: function (res) {
          resolve(res);
        },
        fail: function (res) {
          reject(res);
        }
      });
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    //是否登录
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin) {
      this.setData({
        isLogin: true,
        openid: wx.getStorageSync('openid'),
        avatarUrl: wx.getStorageSync('avatarUrl'),
        nickName: wx.getStorageSync('nickName'),
        userData: wx.getStorageSync('userData'),
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})