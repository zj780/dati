// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl: '', //用户头像
    openid: '', //openid
    nickName: '', //用户昵称
    isLogin: false, //默认未登录
    userData: {}, //用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    //是否登录
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin) {
      this.setData({
        isLogin: true,
        openid: wx.getStorageSync('openid'),
        avatarUrl: wx.getStorageSync('avatarUrl'),
        nickName: wx.getStorageSync('nickName'),
        userData: wx.getStorageSync('userData'),
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },


  //复制微信
  copy: function () {
    wx.setClipboardData({
      data: 'php7770',
      success: function (res) {
        wx.showToast({
          title: '复制成功',
        });
      },
      fail: function (res) {
        wx.showToast({
          title: '复制失败',
        });
      }
    })
  },


})