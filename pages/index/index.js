// pages/index/index.js
const app = getApp();
const db = wx.cloud.database();
const $ = db.command.aggregate;
const _ = db.command;
import util from '../../utils/util';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    openid: '', //openid
    isLogin: false, //默认未登录
    avatarUrl: '', //头像
    nickName: '', //用户昵称
    backgroundImage: '', //背景图片
    userData: {}, //用户信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var that = this;
    //未登录需要通过openid检测用户身份
    var isLogin = wx.getStorageSync('isLogin');
    if (!isLogin) {
      that.getOpenid();
    }
    //获取图片配置
    app.appInt().then(res => {
      //赋值图片数组
      app.globalData.images = res.data[0].images;
      //赋值背景图
      that.setData({
        backgroundImage: util.getImage(app.globalData.images, 'index')
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    //监听用户登录态
    var isLogin = wx.getStorageSync('isLogin');
    if (isLogin) {
      this.setData({
        isLogin: true,
        openid: wx.getStorageSync('openid'),
        avatarUrl: wx.getStorageSync('avatarUrl'),
        nickName: wx.getStorageSync('nickName'),
        userData: wx.getStorageSync('userData'),
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },

  //新版头像
  onChooseAvatar: function (event) {
    //头像 临时链接
    var avatarUrl = event.detail.avatarUrl;
    //缓存头像
    wx.setStorageSync('avatarUrl', avatarUrl);
    //跳转头像设置页面
    wx.navigateTo({
      url: "/pages/setUser/setUser"
    });
  },

  //登录
  login: function () {
    wx.vibrateShort();
    wx.showToast({
      title: '请稍候',
      icon: "loading",
      duration: 1000,
      mask: true
    })
  },

  //获取openid
  getOpenid: function () {
    var that = this;
    //加载中
    wx.showToast({
      title: '加载中',
      icon: "loading",
      duration: 5000,
      mask: true
    })
    //获取openid
    wx.cloud.callFunction({
      name: 'getOpenid',
      data: {},
      success: res => {
        //获取openid
        var openid = res.result.openid;
        that.setData({
          openid: openid
        })
        //缓存openid
        wx.setStorageSync('openid', openid);
        //检测用户是否注册
        that.checkUser();
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
        wx.showToast({
          title: '登录失败！' + err,
          icon: 'none'
        });
      }
    })
  },


  //检测用户是否注册
  checkUser: function () {
    var that = this;
    //云函数检查是否注册
    wx.cloud.callFunction({
      name: "checkUser",
      data: {
        openid: that.data.openid
      },
      success: function (res) {
        //隐藏加载框
        wx.hideToast();
        //判断用户
        if (res.result.success === true) {
          //获取用户信息
          var userData = res.result.data[0];
          //已登录缓存
          wx.setStorageSync('isLogin', true);
          //身份信息缓存
          wx.setStorageSync('avatarUrl', userData.avatarUrl);
          wx.setStorageSync('nickName', userData.nickName);
          wx.setStorageSync('openid', userData.openid);
          wx.setStorageSync('userData', userData);
          //登录状态(已登录)
          that.setData({
            isLogin: true
          })
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '登录失败',
          icon: "none",
          duration: 1000
        })
      }
    });
  },


  //跳转实名页面(用户实名)
  realName: function () {
    //是否填写资料
    
    wx.navigateTo({
      url: "/pages/realName/realName"
    });
  },


})